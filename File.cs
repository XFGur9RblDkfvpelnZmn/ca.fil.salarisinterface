﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections;

namespace SalarisInterface
{
    class File
    {
        public string[] lines;
        protected string path = Parms.sbPath()+@"log\";

        #region constructors

        public File()
        {
            //  LEES BESTAND
            lines = System.IO.File.ReadAllLines(path + "expblox1.txt");
            DateTime now = DateTime.Now;
           System.IO.File.Copy(path + "expblox1.txt", Parms.salInfPath()+@"log\expblox"+now.Year+now.Month+now.Day+now.Hour+now.Minute+now.Second+".txt",true);
        }
        public File(string bestand)
        {
            //  LEES BESTAND
            lines = System.IO.File.ReadAllLines(path + bestand);
        }
        public File(string path, string bestand)
        {
            //  LEES BESTAND
            lines = System.IO.File.ReadAllLines(path + bestand);
        }

        #endregion

        static public void writeFile(ArrayList records, ArrayList werknemers)
        {

            StreamWriter file = new System.IO.StreamWriter(Parms.sbPath()+@"log\expblox.txt");
            //foreach (Record record in records)
            //{
            //    Personeel persoon = Personeel.zoekSalnum(werknemers, record.Salnum);
            //    record.WerkgeverNr = persoon.Team;
            //    if (record.Salnum != "weg")
            //    {
            //        record.Salnum = persoon.Filler1;
            //    }
            //}
            records.Sort();
            foreach (Record record in records)
            {
                Personeel persoon = Personeel.zoekSalnum(werknemers, record.Salnum);
                if (record.Salnum != "weg")
                {
                    
                    file.WriteLine(persoon.Team + persoon.Filler1 + record.Datum + record.LoonCode + record.Uren + "             " + record.Kp);
                }
            }
            
            file.Close();
        }
        
    }
}

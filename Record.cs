﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Globalization;

namespace SalarisInterface
{
    class Record : IComparable
    {
        #region lokale vars

        string werkgeverNr, salarisNr, datum, loonCode, uren, kp;
        int week;
        DateTime dDatum;

        #endregion

        #region sorteren

        public int CompareTo(object obj)
        {
            Record Compare = (Record)obj;
            string sortOn = this.werkgeverNr + this.salarisNr + this.datum;
            string sortOnObject = Compare.werkgeverNr + Compare.salarisNr + Compare.datum;
            int result = sortOn.CompareTo(sortOnObject);
            if (result == 0)
                result = sortOn.CompareTo(sortOnObject);
            return result;
        }

        #endregion

        #region constructor

        public Record(string werkgeverNr, string salarisNr, string datum, string loonCode, string uren, string kp)
        {
            //  STEEK ONTVANGEN VARIABELEN IN OBJECTVARS
            this.werkgeverNr = werkgeverNr;
            this.salarisNr = salarisNr;
            this.datum = datum;
            this.loonCode = loonCode;
            this.uren = uren;
            this.kp = kp;
            if (datum.Length > 0)
            {
                int jaar = Convert.ToInt32("20" + datum.Substring(0, 2));
                int maand = Convert.ToInt32(datum.Substring(2, 2));
                int dag = Convert.ToInt32(datum.Substring(4, 2));
                dDatum = new DateTime(jaar, maand, dag);
                //BEREKEN WEEK VH JAAR
                CultureInfo myCI = new CultureInfo("nl-BE");
                Calendar myCal = myCI.Calendar;

                // Gets the DTFI properties required by GetWeekOfYear.
                CalendarWeekRule myCWR = myCI.DateTimeFormat.CalendarWeekRule;
                DayOfWeek myFirstDOW = myCI.DateTimeFormat.FirstDayOfWeek;

                int week = myCal.GetWeekOfYear(dDatum, myCWR, myFirstDOW);
                DayOfWeek laatsteDag = new DateTime(jaar,12,31).DayOfWeek;
                if ((week == 1) && (laatsteDag != DayOfWeek.Sunday))
                {
                    this.week = myCal.GetWeekOfYear(new DateTime(jaar, 12, 31),myCWR,myFirstDOW);
                }
                else
                {
                    this.week = week;
                }
                if (this.week == 54)
                {
                    this.week = 53;
                }
            }
            else
            {
                this.week = 0;
            }
        }

        #endregion

        #region public vars

        public DateTime DDatum
        {
            get
            {
                return dDatum;
            }
        }
        public string LoonCode
        {
            get
            {
                return loonCode;
            }
        }
        public int Week
        {
            get
            {
                return week;
            }
        }
        public string WerkgeverNr
        {
            get
            {
                return werkgeverNr;
            }
            set
            {
                werkgeverNr = value;
            }
        }
        public string Datum
        {
            get
            {
                return datum;
            }
        }
        public string Uren
        {
            get
            {
                return uren;
            }
            set
            {
                uren = value;
            }
        }
        public string Kp
        {
            get
            {
                return kp;
            }
        }
        public string Salnum
        {
            get
                {
                return salarisNr;
            }
            set
            {
                salarisNr = value;
            }
        }

        #endregion

        #region methods

        public void checkKp(Personeel persoon,ArrayList omzet)
        {
            if ((persoon.Mandant == "DIS") || (persoon.Mandant == ""))
            {
                if ((this.kp == "005") || (this.kp == "006") || (this.kp == "007") || (this.kp == "008") || (this.kp == "009"))
                {
                    this.kp = persoon.Ahvnr.Substring(0, 4) + this.kp;
                }
                else
                {
                    Kp kostP = SalarisInterface.Kp.zoekKp(omzet, this.kp.Trim());
                    if (kostP.nummer.Trim().Length > 0)
                    {
                        this.kp = kostP.nummer;
                    }
                }
            }
            else
            {
                string conv = "";//CONVERSIE
                int length = persoon.Functie.Length;
                if (this.kp.Trim() != persoon.Afd)
                {
                    this.kp = geefNummer(this.kp.Trim(), omzet) + persoon.Functie.Substring(length - 3, 3) + " ";
                }
                else
                {
                    this.kp = "       ";
                }
                

            }
        }
        public void geenUren()
        {
            this.uren = "0000";
        }
        public void zet1013Om(ArrayList records,Personeel persoon)
        {
            int aant1013 = Convert.ToInt32(this.uren);
            foreach (Record record in records)
            {
                if ((record.datum == this.datum) && (record.salarisNr == this.salarisNr) && ((record.loonCode == "1010") || (record.loonCode == "1410")))
                {
                    int temp = (Convert.ToInt32(record.uren) - aant1013);
                    if (temp < 0)
                    {
                        aant1013 = temp * (-1);
                        temp = 0;
                    }
                    else
                    {
                        aant1013 = 0;
                    }
                    record.uren = temp.ToString("0000");
                    
                    while (record.uren.Length < 4)
                    {
                        record.uren = "0" + record.uren;
                    }
                    if (record.uren == "0000")
                    {
                        record.salarisNr = "weg";
                        if (record.kp != persoon.Afd)
                        {
                            this.kp = record.kp;
                        }
                    }
                }
            }
            //  WANNEER GEEN 1010/1410/1049 uren zijn wordt het 1013/1014 record verwijderd
            if (aant1013.ToString("0000") == this.uren)
            {
                this.salarisNr = "weg";
            }
        }
        public Record checkLooncode1049(Personeel persoon, ArrayList records)
        {
            Record newRec = new Record("", "", "", "", "", "");
            if ((this.loonCode == "1049") && (Convert.ToInt32(this.uren) > 800))
            {
                string aant1010 = Convert.ToString(Convert.ToInt32(this.uren) - 800);
                this.uren = "0800";
                while (aant1010.Length < 4)
                {
                    aant1010 = "0" + aant1010;
                }
                newRec = new Record(this.werkgeverNr, this.salarisNr, this.datum, "1010", aant1010, this.kp);
            }
            return newRec;
        }
        public Record checkLooncode(Personeel persoon, ArrayList records,int aant1010)
        {
            Record newRec = new Record("", "", "", "", "", "") ;
            
            if(aant1010>0)
            {
                
                //foreach (Record record in records)
                //{
                //    if ((record.Salnum == persoon.Salnum) && (record.LoonCode == "1010"))
                //    {
                //        aant1010 += Convert.ToInt32(record.Uren);
                //    }
                //}
                //aant1010 = Convert.ToInt32(persoon.Nominaal) - Convert.ToInt32(persoon.Contracturen);
                foreach (Record record in records)
                {
                    
                    if ((record.Salnum == persoon.Salnum) && (record.LoonCode == "1010") && (aant1010>0)&&(record.week == this.week)&&(record.dDatum.DayOfWeek!=DayOfWeek.Sunday))
                    {
                        if (Convert.ToInt32(record.Uren) == aant1010)
                        {
                            record.loonCode = "1046";
                            aant1010 = 0;
                        }
                        else if (Convert.ToInt32(record.Uren) < aant1010)
                        {
                            record.loonCode = "1046";
                            aant1010 = aant1010 - Convert.ToInt32(record.uren);
                        }
                        else
                        {
                            record.uren = Convert.ToString(Convert.ToInt32(record.uren) - aant1010);
                            while (record.uren.Length < 4)
                            {
                                record.uren = "0" + record.uren;
                            }
                            string sAant1010 = Convert.ToString(aant1010);
                            while (sAant1010.Length < 4)
                            {
                                sAant1010 = "0" + sAant1010;
                            }
                            
                            newRec = new Record(record.werkgeverNr, record.salarisNr, record.datum, "1046", sAant1010, record.kp);
                            
                            aant1010 = 0;
                        }
                    }
                }
                persoon.Nominaal = persoon.Contracturen;
            }
           
            return newRec;
        }
        static string geefNummer(string naam, ArrayList conv)
        {
            string nummer = "";
            foreach (Kp kostp in conv)
            {
                if (kostp.naam == naam)
                {
                    nummer = kostp.nummer;
                }
            }
            while ((nummer.Length < 3) && (nummer.Length != 0))
            {
                nummer = "0" + nummer;
            }
            return nummer;
        }
        public void checkContract(Personeel persoon, ArrayList records)
        {
            int totaal = 0;
            foreach (Record record in records)
            {
                
                if ((persoon.Salnum == record.salarisNr) && (this.week == record.week) && (record.loonCode != "9876") && (record.salarisNr != "weg"))
                {
                    totaal += Convert.ToInt32(record.uren);                    
                }
                
            }
            if ((persoon.Mandant == "DIS") && (totaal > Convert.ToInt32(persoon.Contracturen) / 60 * 100) && (this.loonCode == "1010"))
            {
                if ((totaal - Convert.ToInt32(persoon.Contracturen) / 60 * 100) >= Convert.ToInt32(this.uren))
                {
                    this.uren = "0000";
                }
                else if ((totaal - Convert.ToInt32(persoon.Contracturen) / 60 * 100) < Convert.ToInt32(this.uren))
                {
                    this.uren = (Convert.ToInt32(this.uren) - (totaal - Convert.ToInt32(persoon.Contracturen) / 60 * 100)).ToString("0000");
                }

            }
        }
        public void checkRoulement(Personeel persoon, ArrayList records)
        {
            try
            {
                int totaal = 0;
                //Record newRec = new Record("", "", "", "", "", "") ;
                if (persoon.Vt == "1")
                {
                    //DateTime[] data = new DateTime[7];
                    Record[] week = new Record[7];
                    totaal = 0;

                    foreach (Record record in records)
                    {
                        if ((persoon.Salnum == record.salarisNr) && (this.week == record.week) && (record.loonCode != "9876") && (record.salarisNr != "weg"))
                        {
                            totaal += Convert.ToInt32(record.uren);
                            int jaar = Convert.ToInt32("20" + record.datum.Substring(0, 2));
                            int maand = Convert.ToInt32(record.datum.Substring(2, 2));
                            int dag = Convert.ToInt32(record.datum.Substring(4, 2));
                            DateTime datum = new DateTime(jaar, maand, dag);
                            switch (datum.DayOfWeek)
                            {
                                case DayOfWeek.Monday:
                                    //data[0] = datum;
                                    if (week[0] == null)
                                    {
                                        week[0] = record;
                                    }
                                    else if (Convert.ToInt32(week[0].uren) == 0)
                                    {
                                        week[0] = record;
                                    }
                                    break;
                                case DayOfWeek.Tuesday:
                                    //data[1] = datum;
                                    if (week[1] == null)
                                    {
                                        week[1] = record;
                                    }
                                    else if (Convert.ToInt32(week[1].uren) == 0)
                                    {
                                        week[1] = record;
                                    }
                                    break;
                                case DayOfWeek.Wednesday:
                                    //data[2] = datum;
                                    if (week[2] == null)
                                    {
                                        week[2] = record;
                                    }
                                    else if (Convert.ToInt32(week[2].uren) == 0)
                                    {
                                        week[2] = record;
                                    }
                                    break;
                                case DayOfWeek.Thursday:
                                    //data[3] = datum;
                                    if (week[3] == null)
                                    {
                                        week[3] = record;
                                    }
                                    else if (Convert.ToInt32(week[3].uren) == 0)
                                    {
                                        week[3] = record;
                                    }
                                    break;
                                case DayOfWeek.Friday:
                                    //data[4] = datum;
                                    if (week[4] == null)
                                    {
                                        week[4] = record;
                                    }
                                    else if (Convert.ToInt32(week[4].uren) == 0)
                                    {
                                        week[4] = record;
                                    }
                                    break;
                                case DayOfWeek.Saturday:
                                    //data[5] = datum;
                                    if (week[5] == null)
                                    {
                                        week[5] = record;
                                    }
                                    else if (Convert.ToInt32(week[5].uren) == 0)
                                    {
                                        week[5] = record;
                                    }
                                    break;
                                case DayOfWeek.Sunday:
                                    //data[6] = datum;
                                    if (week[6] == null)
                                    {
                                        week[6] = record;
                                    }
                                    else if (Convert.ToInt32(week[6].uren) == 0)
                                    {
                                        week[6] = record;
                                    }
                                    break;
                            }

                        }
                    }
                    int aantal = 0;
                    int vrij = 0;
                    int vol = 0;
                    for (int i = 0; i < 7; i++)
                    {
                        try
                        {
                            if (Convert.ToInt32(week[i].uren) > 0)
                            {
                                aantal++;
                                vol = i;
                            }
                            else if ((i != 5) && (i != 6))
                            {
                                vrij = i;
                            }
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                    //DateTime vrijeDag = data[vol];
                    //vrijeDag = vrijeDag.AddDays(vrij - vol);
                   
                    if ((aantal == 4) && (totaal >= 3500))
                    {
                        //newRec.werkgeverNr = werkgeverNr;
                        //newRec.salarisNr = salarisNr;
                        //newRec.loonCode = "9450";
                        //newRec.uren = "0000";
                        //newRec.kp = "       ";
                        //string jaar = Convert.ToString(vrijeDag.Year).Substring(2,2);
                        //string maand = Convert.ToString(vrijeDag.Month);
                        //if (vrijeDag.Month < 10)
                        //{
                        //    maand = "0" + maand;
                        //}
                        //string dag = Convert.ToString(vrijeDag.Day);
                        //if (vrijeDag.Day < 10)
                        //{
                        //    dag = "0" + dag;
                        //}
                        //newRec.datum = jaar + maand + dag;
                        try
                        {
                        week[vrij].loonCode = "9450";
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                    

                }

            }
            catch(Exception ex)
            {

            }
            //return newRec;
        }
        public void removeUren(Personeel persoon, ArrayList records)
        {
            int aant1010 = Convert.ToInt32(this.uren);
            foreach (Record record in records)
            {
                if ((record.Salnum == persoon.Salnum) && (record.LoonCode == "1010") && (aant1010 > 0)&&(this.week == record.week))
                {
                    if (Convert.ToInt32(record.Uren)-300 == aant1010)
                        {
                            record.uren = "0300";
                            aant1010 = 0;
                        }
                        else if (Convert.ToInt32(record.Uren)-300 < aant1010)
                        {
                            if (Convert.ToInt32(record.Uren) > 300)
                            {
                                aant1010 = aant1010 - (Convert.ToInt32(record.Uren) - 300);
                                record.uren = "0300";
                            }
                        }
                        else
                        {
                            record.uren = Convert.ToString(Convert.ToInt32(record.uren) - aant1010);
                            while (record.uren.Length < 4)
                            {
                                record.uren = "0" + record.uren;
                            }
                            
                            aant1010 = 0;
                        }
                }
            }
            if (this.bestaatRecordZelfdeDag(records))
            {
                this.salarisNr = "weg";
            }
            else
            {
                this.uren = "0000";
                this.loonCode = "0000";
            }
            
        }
        public ArrayList omBoek1041(Personeel persoon, ArrayList records)
        {
            ArrayList omgeboekt = new ArrayList();

            int aant1010 = Convert.ToInt32(this.uren);
            foreach (Record record in records)
            {
                if ((record.Salnum == persoon.Salnum) && (record.LoonCode == "1010") && (aant1010 > 0) && (this.week == record.week)&&(record.DDatum.DayOfWeek!=DayOfWeek.Sunday))
                {
                    if (Convert.ToInt32(record.Uren) == aant1010)
                    {
                        record.uren = "0000";
                        record.loonCode = "0000";
                        Record nieuw = new Record(record.werkgeverNr,record.salarisNr,record.datum,this.loonCode,aant1010.ToString("0000"),record.kp);
                        omgeboekt.Add(nieuw);
                        aant1010 = 0;
                    }
                    else if (Convert.ToInt32(record.Uren) < aant1010)
                    {
                        
                        aant1010 = aant1010 - (Convert.ToInt32(record.Uren));
                        int aant1041 = (Convert.ToInt32(record.Uren));
                        record.uren = "0000";
                        record.loonCode = "0000";
                        Record nieuw = new Record(record.werkgeverNr, record.salarisNr, record.datum, this.loonCode, aant1041.ToString("0000"), record.kp);
                        omgeboekt.Add(nieuw);
                    
                    }
                    else
                    {
                        record.uren = Convert.ToString(Convert.ToInt32(record.uren) - aant1010);
                        while (record.uren.Length < 4)
                        {
                            record.uren = "0" + record.uren;
                        }
                        Record nieuw = new Record(record.werkgeverNr, record.salarisNr, record.datum, this.loonCode, aant1010.ToString("0000"), record.kp);
                        omgeboekt.Add(nieuw);
                        aant1010 = 0;
                    }
                }
                
            }
            this.uren = "0000";
            this.loonCode = "0000";
            return omgeboekt;
        }
        public void checkDubbel(ArrayList records)
        {
            foreach (Record record in records)
            {
                if ((this.datum == record.datum) && (this.kp == record.kp) && (this.loonCode == record.loonCode) && (this.salarisNr == record.salarisNr) && (this.werkgeverNr == record.werkgeverNr))
                {
                    string salnum = this.salarisNr;
                    this.salarisNr = "weg";
                    if (record.salarisNr == "weg")
                    {
                        this.Salnum = salnum;
                    }
                    else
                    {
                        int uren = Convert.ToInt32(record.uren);
                        int bijtel = Convert.ToInt32(this.uren);
                        uren = uren + bijtel;
                        record.uren = uren.ToString("0000");
                    }
                }
                
            }
        }
        public Boolean bestaatRecordZelfdeDag(ArrayList records)
        {
            foreach (Record record in records)
            {
                string salnum = this.salarisNr;
                // CHECK OF THIS HETZELFDE RECORD IS ALS RECORD
                //zet salarisnummer van this op "weg"
                this.salarisNr = "weg";
                int checkZelfde = 1;
                //als het salarisnummer van record niet gelijk is aan "weg" gaat het om een ander record
                if (record.salarisNr != "weg")
                {
                    checkZelfde = 0;
                }
                //salnum terug op originele plaatsen
                this.Salnum = salnum;
                if ((checkZelfde==0)&&(record.salarisNr == this.salarisNr) && (record.werkgeverNr == this.werkgeverNr) && (record.datum == this.datum))
                {
                    return true;
                }
                
            }
            return false;
        }
        #endregion
    }
}

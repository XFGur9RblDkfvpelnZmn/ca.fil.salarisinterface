﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace SalarisInterface
{
    class Wochsoll
    {
        public string salnum;
        public int wochsoll, filler2,week;

        public Wochsoll(string salnum, int wochsoll, int filler2,int week)
        {
            this.salnum = salnum;
            this.wochsoll = wochsoll;
            this.filler2 = filler2;
            this.week = week;
        }
        static public int checkBijlageContract(ArrayList wochsoll,int week, string salnum)
        {
            Wochsoll woch = zoekSalnumWeek(wochsoll, week, salnum);
            return (woch.wochsoll - woch.filler2)/60*100;
        }
        static public Wochsoll zoekSalnumWeek(ArrayList wochsoll,int week, string salnum)
        {
            Wochsoll ret = new Wochsoll("", 0, 0, 0);
            foreach (Wochsoll woch in wochsoll)
            {
                if ((woch.salnum == salnum)&&(woch.week==week))
                {
                    ret = woch;
                }
            }
            return ret;
        }
       
        
       
    }
}

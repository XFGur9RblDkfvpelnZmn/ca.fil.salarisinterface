﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.IO;
using System.Globalization;

namespace SalarisInterface
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                // KOPIEER CONVERSIETABEL NAAR LOG FOLDER
                List<string> cmds = new List<string>();
                //cmds.Add("TC_MANDANT=DIS");
                cmds.Add("sqlsel -n11 -d; -s -o"+Parms.sbPath()+"log/personeel.csv");
                //Proces.cmd(cmds);
                //cmds.Clear();
                System.IO.File.Copy(Parms.salInfPath() + @"filkp.csv", Parms.sbPath() + "log\\" + @"filkp.csv", true);

                #region weeknominalen

                // CHECK HOEVEEL WEKEN IN JAAR
                DateTime dDatum = new DateTime(DateTime.Now.Year, 12, 31);
                //BEREKEN WEEK VH JAAR
                CultureInfo myCI = new CultureInfo("nl-BE");
                Calendar myCal = myCI.Calendar;

                // Gets the DTFI properties required by GetWeekOfYear.
                CalendarWeekRule myCWR = myCI.DateTimeFormat.CalendarWeekRule;
                DayOfWeek myFirstDOW = myCI.DateTimeFormat.FirstDayOfWeek;
                // WEEK VH JAAR
                int week = myCal.GetWeekOfYear(dDatum, CalendarWeekRule.FirstFourDayWeek, myFirstDOW);

                // MAAK EEN ARRAYLIST VOOR HET VERGELIJKEN VAN WEEKNOMINAAL EN
                // CONTRACTUREN PER WEEK
                ArrayList wochsoll = new ArrayList();

                // VOOR ELKE WEEK VAN HET JAAR
                for (int i = 1; i <= week; i++)
                {
                    // MAAK EEN FILE AAN VOOR ELKE WEEK VH JAAR
                    File weekn = new File(Parms.salInfPath() + @"wochsoll\", "week" + i + ".csv");
                    foreach (string line in weekn.lines)
                    {
                        // STEEK DE DATA IN EEN OBJECT WOCHSOLL EN VOEG TOE AAN ARRAYLIST
                        string[] data = line.Split(';');
                        if ((data[0].Trim().Length > 0) && (data[1].Trim().Length > 0) && (data[2].Trim().Length > 0))
                        {
                            Wochsoll nieuw = new Wochsoll(data[0].Trim(), Convert.ToInt32(data[1].Trim()), Convert.ToInt32(data[2].Trim()), i);
                            wochsoll.Add(nieuw);
                        }

                    }
                }

                #endregion

                #region wplekcodes

                //List<string> cmds = new List<string>();
                cmds.Add("sqlsel \"wplekkod,filler from werkplek \" > \"" + Parms.sbPath() + "log\\" + "diskp.csv\" -d; -s");
                Proces.cmd(cmds);

                #endregion

                #region bestanden

                //  CREEER OBJECT SALARISINTERFACE EN PERSONEEL
                File salarisInterface = new File();
                File personeel = new File("personeel.csv");
                File filKpOmzet = new File("filkp.csv");
                File disKpOmzet = new File("diskp.csv");

                #endregion

                #region arraylists

                //  CREEER ARRAYLIST VOOR LIJNEN VANUIT SALARISINTERFACE
                ArrayList records = new ArrayList();
                ArrayList werknemers = new ArrayList();
                ArrayList filKp = new ArrayList();
                ArrayList disKp = new ArrayList();
                //DateTime nu = DateTime.Now();

                #endregion

                #region objecten vullen en toevoegen aan arraylists

                //  VOOR ELKE LIJN IN SALARISINTERFACE
                foreach (string line in salarisInterface.lines)
                {
                    //  SPLITS LIJN IN DATA
                    string werkgeverNr = line.Substring(0, 7);
                    string salarisNr = line.Substring(7, 7);
                    string datum = line.Substring(14, 6);
                    string loonCode = line.Substring(20, 4);
                    string uren = line.Substring(24, 4);
                    string kp = line.Substring(41, 7);

                    //  MAAK NIEW OBJECT LIJN
                    Record lijn = new Record(werkgeverNr, salarisNr, datum, loonCode, uren, kp);

                    //  STEEK BIJ IN ARRAYLIST
                    records.Add(lijn);

                }

                //  VOOR ELKE LIJN IN PERSONEEL
                foreach (string line in personeel.lines)
                {
                    //  SPLITS LIJN IN DATA
                    string[] data = line.Split(';');
                    int lengte;
                    if (data[9].Trim().Length - 3 > 0)
                    {
                        lengte = data[9].Trim().Length - 3;
                    }
                    else
                    {
                        lengte = 0;
                    }
                    //  MAAK NIEW OBJECT PERSONEEL
                    Personeel pers = new Personeel(data[0].Trim(), data[1].Trim(), data[2].Trim(), data[3].Trim(), data[4].Trim(), data[5].Trim(), data[6].Trim(), data[7].Trim(), data[8].Trim(), data[9].Trim().Substring(lengte), data[10].Trim(), data[11].Trim(), data[12].Trim());

                    //  STEEK BIJ IN ARRAYLIST
                    werknemers.Add(pers);

                }

                //  VOOR ELKE LIJN IN filkp
                foreach (string line in filKpOmzet.lines)
                {
                    //  SPLITS LIJN IN DATA
                    string[] data = line.Split(';');

                    //  MAAK NIEW OBJECT PERSONEEL
                    Kp kostp = new Kp(data[0].Trim(), data[1].Trim());

                    //  STEEK BIJ IN ARRAYLIST
                    filKp.Add(kostp);

                }

                //  VOOR ELKE LIJN IN diskp
                foreach (string line in disKpOmzet.lines)
                {
                    //  SPLITS LIJN IN DATA
                    string[] data = line.Split(';');

                    //  MAAK NIEW OBJECT PERSONEEL
                    Kp kostp = new Kp(data[0].Trim(), data[1].Trim());

                    //  STEEK BIJ IN ARRAYLIST
                    disKp.Add(kostp);

                }

                #endregion

                #region aanpassingen

                //  ARRAYLIST VOOR TOEVOEGEN NIEUWE RECORDS
                ArrayList newRec = new ArrayList();

                //  VARS OM TE CHECKEN OF BIJLAGECONTRACT VOOR DIE WEEK REEDS BEREKEND IS
                int lastWeekBijlage = 0;
                string lastSalnumBijlage = "";

                //  VOOR ELK OBJECT IN RECORDS
                foreach (Record record in records)
                {
                    record.checkDubbel(records);
                    //  ZOEK DE JUISTE PERSOON BIJ HET RECORD
                    Personeel persoon = Personeel.zoekSalnum(werknemers, record.Salnum);

                    //  ZET UREN OP 0 BIJ LOONCODE 9450 en 9998
                    if ((record.LoonCode == "9450") || (record.LoonCode == "9998"))
                    {
                        record.geenUren();
                    }
                    //  WANNEER LOONCODE 1013of1014 zet 1010 UREN OM
                    if ((record.LoonCode == "1013") || (record.LoonCode == "1014") || (Convert.ToInt32(record.LoonCode) >= 8042) && (Convert.ToInt32(record.LoonCode) <= 8044) || (Convert.ToInt32(record.LoonCode) >= 1042) && (Convert.ToInt32(record.LoonCode) <= 1044) || Convert.ToInt32(record.LoonCode) == 8048)
                    {
                        record.zet1013Om(records, persoon);
                    }
                    //  VERANDER LOONCODE BIJ BIJLAGECONTRACT
                    int bijlage = Wochsoll.checkBijlageContract(wochsoll, record.Week, record.Salnum);
                    if ((bijlage > 0) && ((lastWeekBijlage != record.Week) || (lastSalnumBijlage != persoon.Salnum)))
                    {
                        Record newCheck = record.checkLooncode(persoon, records, bijlage);
                        if (newCheck.LoonCode != "")
                        {
                            newRec.Add(newCheck);
                        }
                        lastSalnumBijlage = persoon.Salnum;
                        lastWeekBijlage = record.Week;
                    }
                    // HAAL CODE 9876 WEG EN HAAL EVENVEEL 1010 uren WEG.
                    if (record.LoonCode == "9876")
                    {
                        record.removeUren(persoon, records);
                    }
                    // WANNEER LOONCODE 1041 OP ZONDAG VOORKOMT, ZET DEZE OP EEN ANDERE DAG EN HAAL 1010 weg
                    else if (((record.LoonCode == "1041") || (record.LoonCode == "1045") || (record.LoonCode == "8045")) && (record.DDatum.DayOfWeek == DayOfWeek.Sunday))
                    {
                        newRec.AddRange(record.omBoek1041(persoon, records));
                    }
                    //  MAX. 8u OP LOONCODE 1049
                    //Record newChecks = record.checkLooncode1049(persoon, records);
                    //if (newChecks.LoonCode != "")
                    //{
                    //    newRec.Add(newChecks);
                    //}

                    //if (persoon.Mandant == "FIL")
                    {
                        //Record newCheck = record.checkRoulement(persoon, records);
                        record.checkRoulement(persoon, records);
                        //if (newCheck.LoonCode != "")
                        //{
                        //    newRec.Add(newCheck);
                        //}
                    }

                }
                // NIEUWE RECORDS TOEVOEGEN AAN ARRAYLIST
                records.AddRange(newRec);

                foreach (Record record in records)
                {
                    Personeel persoon = Personeel.zoekSalnum(werknemers, record.Salnum);
                    if (persoon.Mandant == "FIL")
                    {
                        //  VERANDER KOSTENPLAATS
                        record.checkKp(persoon, filKp);
                    }
                    else
                    {
                        record.checkContract(persoon, records);
                        //  VERANDER KOSTENPLAATS DC
                        record.checkKp(persoon, disKp);
                    }
                    // DUBBELE RECORDS VERWIJDEREN (bv 0-records)
                    record.checkDubbel(records);
                    // LOONCODE 1010 (of 0000) MET 0000 UREN ENKEL LATEN STAAN INDIEN GEEN ANDER RECORD OP ZELFDE DAG
                    if (((record.LoonCode == "1010") || (record.LoonCode == "0000")) && (record.Uren == "0000") && (record.bestaatRecordZelfdeDag(records)))
                    {
                        record.Salnum = "weg";
                    }
                }

                #endregion

                // SCHRIJF DE GEGEVENS WEG NAAR DE NIEUWE FILE
                File.writeFile(records, werknemers);
            }
            catch (Exception ex)
            {
                List<String> cmds = new List<string>();
                cmds.Add("clnt_exp event_log CEA \"De salarisinterface genereerde een onbekende uitzondering: "+ex.Message+"\"");
                Proces.cmd(cmds);
            }
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace SalarisInterface
{
    class Kp
    {
        public string naam, nummer;

        public Kp(string naam, string nummer)
        {
            this.naam = naam;
            this.nummer = nummer;
        }
        public static Kp zoekKp(ArrayList kpList, string nummer)
        {
            Kp ret = new Kp("", "");
            foreach (Kp kostP in kpList)
            {
                if (kostP.naam == nummer)
                {
                    ret = kostP;
                }
            }
            return ret;
        }
    }
}

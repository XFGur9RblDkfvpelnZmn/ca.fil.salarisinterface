﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;

namespace SalarisInterface
{
    class Proces
    {
        #region Fields

        #endregion

        #region Constructors

        /*
		 * The default constructor
 		 */
        public Proces()
        {
        }

        #endregion

        #region Properties

        #endregion

        #region Methods
        static public void cmd(List<String> args)
        {
            TextWriter tw = new StreamWriter(Parms.sbPath() + "log/tmp.bat");

            foreach (string arg in args)
            {
                tw.WriteLine(arg);
            }
            tw.Close();

            //definieer tmp.bat
            ProcessStartInfo p = new System.Diagnostics.ProcessStartInfo(Parms.sbPath() + "log/tmp.bat");
            Process proces = new System.Diagnostics.Process();
            proces.StartInfo = p;

            //start bat file
            proces.Start();
            proces.WaitForExit();
        }
        #endregion

    }
}

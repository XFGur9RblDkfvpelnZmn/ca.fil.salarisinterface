﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace SalarisInterface
{
    class Personeel
    {
        #region lokale vars

        string vt,naam, salnum, mandant, ahvnr, functie, nominaal, contracturen,team,filler1,kp,afd,salinf;

        #endregion

        #region public vars

        public string Salnum
        {
            get
            {
                return salnum;
            }
        }
        public string Salinf
        {
            get
            {
                return salinf;
            }
        }
        public string Vt
        {
            get
            {
                return vt;
            }
            set
            {
                vt = value;
            }
        }
        public string Mandant
        {
            get
            {
                return mandant;
            }
        }
        public string Afd
        {
            get
            {
                return afd;
            }
        }
        public string Ahvnr
        {
            get
            {
                return ahvnr;
            }
        }
        public string Functie
        {
            get
            {
                return functie;
            }
        }
        public string Nominaal
        {
            get
            {
                return nominaal;
            }
            set
            {
                nominaal = value;
            }
        }
        public string Contracturen
        {
            get
            {
                return contracturen;
            }
        }
        public string Team
        {
            get
            {
                return team;
            }
        }
        public string Filler1
        {
            get
            {
                return filler1;
            }
        }
        public string Kp
        {
            get
            {
                return kp;
            }
        }

        #endregion

        #region constructor

        public Personeel(string naam, string salnum, string mandant, string ahvnr, string functie, string nominaal, string contracturen,string team, string filler1,string kp, string afd,string vt,string salinf)
        {
            this.naam = naam;
            this.salnum = salnum;
            this.mandant = mandant;
            this.ahvnr = ahvnr;
            this.functie = functie;
            this.nominaal = nominaal;
            this.contracturen = contracturen;
            this.team = team;
            this.afd = afd;
            this.vt = vt;
            this.salinf = salinf;
            for (int i = this.team.Length; i < 7; i++)
            {
                this.team = "0" + this.team;
            }
            this.filler1 = filler1;
            for (int i = this.filler1.Length; i < 7; i++)
            {
                this.filler1 = "0" + this.filler1;
            }
            this.kp = kp;
        }

        #endregion

        static public Personeel zoekSalnum(ArrayList personeel, string salnum)
        {
            Personeel ret = new Personeel("", "", "", "", "", "", "","","","","","","");
            foreach (Personeel pers in personeel)
            {
                if (pers.salnum == salnum)
                {
                    ret = pers;
                }
            }
            return ret;
        }
    }
}
